import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';

import { Store, Select } from '@ngxs/store';
import { ChangeScore, ScoreState } from "../store/scoring.store";
import { GameState, ChangeCommonScreen } from "../store/game.store";

import { HttpClient } from '@angular/common/http';

import {Observable} from 'rxjs';

import {localServer} from '../../app.config';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  @Select(ScoreState) score$: Observable<string[]>;
  @Select(GameState) game$: Observable<string[]>;

  constructor(private socket: Socket, private store: Store, private http:HttpClient) {
    this.initStates();
    this.receiveScore().subscribe(message => {
      this.store.dispatch(new ChangeScore(message))
    })

    this.reveiveGameParameters().subscribe(message => {
      this.store.dispatch(new ChangeCommonScreen(message))
    })

  }

  receiveScore(){
    return this.socket.fromEvent('score');
  }

  reveiveGameParameters(){
    return this.socket.fromEvent('gameParameters');
  }

  initStates(){
    // Its very dirty like that...need improvement.
    this.http.get(localServer + 'game/parameters').subscribe(val => {
      this.store.dispatch(new ChangeCommonScreen(val));
    });
    this.http.get(localServer + 'game/point').subscribe(val => {
      this.store.dispatch(new ChangeScore(val));
    });
  }
}
