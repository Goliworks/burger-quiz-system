import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonScreenComponent } from './common-screen.component';
import { QuestionComponent } from './question/question.component';
import { JingleComponent } from './jingle/jingle.component';


@NgModule({
  declarations: [CommonScreenComponent, QuestionComponent, JingleComponent],
  imports: [
    CommonModule
  ],
  exports: [CommonScreenComponent]
})
export class CommonScreenModule { }
