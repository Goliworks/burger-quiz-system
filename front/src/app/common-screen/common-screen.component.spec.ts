import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonScreenComponent } from './common-screen.component';

describe('CommonScreenComponent', () => {
  let component: CommonScreenComponent;
  let fixture: ComponentFixture<CommonScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
