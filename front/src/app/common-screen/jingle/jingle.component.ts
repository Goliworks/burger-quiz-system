import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, Input } from '@angular/core';

@Component({
  selector: 'app-jingle',
  templateUrl: './jingle.component.html',
  styleUrls: ['./jingle.component.scss']
})
export class JingleComponent implements OnInit, AfterViewInit {

  @Input() jingle: string;

  @ViewChild("video", {static: false}) videoDom: ElementRef;

  video: any;

  constructor() { }

  ngOnInit() {
    
  }
  ngAfterViewInit(){
    this.video = this.videoDom.nativeElement;
    this.video.src = "/assets/jingles/"+this.jingle+".mp4";
    this.video.play();
    console.log(this.videoDom.nativeElement);
  }
}
