import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JingleComponent } from './jingle.component';

describe('JingleComponent', () => {
  let component: JingleComponent;
  let fixture: ComponentFixture<JingleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JingleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
