import { Component, OnInit } from '@angular/core';
import { GameService } from '../services/game.service';

@Component({
  selector: 'app-common-screen',
  templateUrl: './common-screen.component.html',
  styleUrls: ['./common-screen.component.scss']
})
export class CommonScreenComponent implements OnInit {

  gameParameters: any;

  constructor(private gameService: GameService) { }

  ngOnInit() {
    this.gameService.game$.subscribe(v => {
      this.gameParameters = v;
      console.log(v);
    })
  }
}
