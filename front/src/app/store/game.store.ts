import { State, Action, StateContext } from '@ngxs/store';

export class ChangeCommonScreen {
  static readonly type = "[Todo] change common screen displaying";
  constructor(public payload: any){}
}

export interface GameStateModel {
  commonScreen: boolean;
  currentTeam: string;
  mode: string,
  question: string,
  selections: string[];
  selected: number;
  display: number;
  answer: string;
  validated: boolean;
  jingle: string;
}

@State<GameStateModel>({
  name: 'game',
  defaults: {
    commonScreen: false,
    currentTeam: 'ketchup',
    mode: '',
    question: '',
    selections: [],
    selected: -1,
    display: -1,
    answer: '',
    validated: false,
    jingle: ''
  }
})
export class GameState {
  @Action(ChangeCommonScreen)
  changeCommonScreen(ctx: StateContext<GameStateModel>, action: ChangeCommonScreen){
    ctx.patchState({
      ...action.payload
    })
  }
}