import { State, Action, StateContext } from '@ngxs/store';

export class ChangeScore {
  static readonly type = "[Todo] change score";
  constructor(public payload: any){}
}

export interface ScoreStateModel {
  ketchup: number;
  mayo: number;
}

@State<ScoreStateModel>({
  name: 'score',
  defaults: {
    ketchup: 0,
    mayo: 0
  }
})
export class ScoreState {
  @Action(ChangeScore)
  changeScore(ctx: StateContext<ScoreStateModel>, action: ChangeScore){
    ctx.patchState({
      ...action.payload
    })
  }
}