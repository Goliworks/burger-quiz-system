import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JingleButtonComponent } from './jingle-button.component';

describe('JingleButtonComponent', () => {
  let component: JingleButtonComponent;
  let fixture: ComponentFixture<JingleButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JingleButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JingleButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
