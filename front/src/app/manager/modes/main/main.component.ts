import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { localServer } from '../../../../app.config';

@Component({
  selector: 'app-ma-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  constructor(private http: HttpClient) { }

  resetGame(){
    this.http.post(localServer + 'game/reset', {}, this.httpOptions).subscribe();
  }

  ngOnInit() {
  }

}
