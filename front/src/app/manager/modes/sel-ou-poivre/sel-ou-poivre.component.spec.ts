import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelOuPoivreComponent } from './sel-ou-poivre.component';

describe('SelOuPoivreComponent', () => {
  let component: SelOuPoivreComponent;
  let fixture: ComponentFixture<SelOuPoivreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelOuPoivreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelOuPoivreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
