import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { localServer } from '../../../app.config';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {

  @Input() gameParameters: any;
  @Output() mode = new EventEmitter<string>();

  modes: Modes[] = [
    {value: 'menu-principal', viewValue: 'Menu principal'},
    {value: 'nuggets', viewValue: 'Nuggets'},
    {value: 'sel-ou-poivre', viewValue: 'Sel ou Poivre'},
    {value: 'menus', viewValue: 'Menus'},
    {value: 'addition', viewValue: "L'addition"},
    {value: 'burger-de-la-mort', viewValue: 'Burger de la mort'}
  ]

  constructor(private http: HttpClient) { }

  changeCommonScreen(){
    this.http.post(localServer + 'game/commonscreen', {}).subscribe();
  }

  changeMode(evt:any){
    this.mode.emit(evt.value);
  }

  ngOnInit() {
  }

}

interface Modes {
  value: string;
  viewValue: String;
}