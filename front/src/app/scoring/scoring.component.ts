import { Component, OnInit, Input, OnChanges, ViewChild, ElementRef, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-scoring',
  templateUrl: './scoring.component.html',
  styleUrls: ['./scoring.component.scss']
})
export class ScoringComponent implements OnInit, OnChanges, AfterViewInit {

  @Input() team: string;
  @Input() score: number;

  @ViewChild('videoWin', {static: false}) videoDom: ElementRef;

  images: miamImage[] = [];
  video: any;

  videoDisplay ='none';

  constructor() { }

  ngOnInit() {

    // Preload images.
    for(let i = 0; i <= 25; i++){
      this.images[i] = new miamImage();
      this.images[i].image = new Image();
      this.images[i].image.src = 'assets/miams/frame_' + this.formatedScore(i) + '_delay-1s.gif';
      // console.log('assets/miams/frame_' + this.formatedScore(i) + '_delay-1s.gif')
      if(i){
        this.images[i].opacity = 0;
      }
    }
  }

  formatedScore(score: number): string {
    if(score < 10){
      return `0${score}`;
    } else {
      return String(score);
    }
  }

  ngOnChanges(){
    for(let i in this.images){
      if(Number(i) <= this.score){
        this.images[i].opacity = 1;
      }
      else{
        this.images[i].opacity = 0;
      }
    }
    if(this.score == 25){
      this.video.currentTime = 0.23;
      setTimeout( () => {
        this.videoDisplay = 'block';
        this.video.play();
      },1000);
    }
    else{
      this.videoDisplay = 'none';
    }
  }

  ngAfterViewInit(){
    this.video = this.videoDom.nativeElement;
    this.video.src = "/assets/jingles/win.mp4";
  }
}

class miamImage {
  image: HTMLImageElement;
  opacity: number;
}