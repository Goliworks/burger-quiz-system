import { Injectable } from '@nestjs/common';
import { GameGateway } from './game.gateway';

@Injectable()
export class GameService {
  
  // go to initGame method.
  score: {[key:string]:number} = {};
  gameParameters: {[key:string]:any} = {};

  private limit = 25;

  constructor(private gameGateway: GameGateway){
    this.initGame();
  }

  changeScore(team: string, type: string){
    
    if(type === ScoreType.Add && this.score[team] < this.limit){
      this.score[team]++;
    } else if(type === ScoreType.Remove && this.score[team] > 0){
      this.score[team]--;
    } else {} 
    this.gameGateway.sendScore(this.score);
  }

  changeCommonScreen(){
    this.gameParameters['commonScreen'] = !this.gameParameters['commonScreen'];
    this.gameGateway.sendGameParameters(this.gameParameters);
  }

  changeCurrentTeam(team: string){
    this.gameParameters['currentTeam'] = team;
    this.gameGateway.sendGameParameters(this.gameParameters);
  }

  changeMode(mode: string,question: string,answer: string,selections: string[]){
    this.gameParameters['mode'] = mode;
    this.gameParameters['selections'] = selections;
    this.gameParameters['answer'] = answer;
    this.gameParameters['question'] = question;
    this.gameParameters['commonScreen'] = true;
    this.gameParameters['display'] = -1;
    this.gameParameters['selected'] = -1;
    this.gameParameters['validated'] = false;
    this.gameGateway.sendGameParameters(this.gameParameters);
  }

  changeSelection(selected: number){
    this.gameParameters['selected'] = selected;
    this.gameGateway.sendGameParameters(this.gameParameters);
  }

  changeDisplay(display: number){
    this.gameParameters['display'] = display;
    this.gameGateway.sendGameParameters(this.gameParameters);
  }

  validAnswer(){
    this.gameParameters['validated'] = true;
    this.gameGateway.sendGameParameters(this.gameParameters);
  }

  launchJingle(jingle: string){
    this.gameParameters['mode'] = 'jingle';
    this.gameParameters['commonScreen'] = true;
    this.gameParameters['jingle'] = jingle;
    this.gameGateway.sendGameParameters(this.gameParameters);
  }

  resetGame(){
    this.initGame();
    this.gameGateway.sendGameParameters(this.gameParameters);
    this.gameGateway.sendScore(this.score)
  }

  private initGame(){
    this.gameParameters = {'commonScreen': false,
    'currentTeam': 'ketchup',
    'mode': '',
    'selections': [],
    'selected': -1,
    'answer': '',
    'display': -1,
    'question': '',
    'validated': false,
    'jingle': ''};

    this.score = {'ketchup': 0, 'mayo': 0};
  }

}

enum ScoreType {
  Add = 'add',
  Remove = 'remove'
}