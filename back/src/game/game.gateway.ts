import { SubscribeMessage, WebSocketServer, WebSocketGateway } from '@nestjs/websockets';

@WebSocketGateway()
export class GameGateway {

  @WebSocketServer() server: any;

  async sendScore(score: any){
    this.server.emit('score', score);
  }

  async sendGameParameters(gp: any){
    this.server.emit('gameParameters', gp);
  }

  @SubscribeMessage('message')
  handleMessage(client: any, payload: any): string {
    return 'Hello world!';
  }
}
