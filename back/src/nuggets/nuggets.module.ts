import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { NuggetsSchema } from './nuggets.model'
import { NuggetsController } from './nuggets.controller';
import { NuggetsService } from './nuggets.service';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Nuggets', schema: NuggetsSchema }])],
  controllers: [NuggetsController],
  providers: [NuggetsService],
})
export class NuggetsModule {}
