import { Model } from 'mongoose';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { NuggetsModel } from './nuggets.model';

@Injectable()
export class NuggetsService {
  constructor(
    @InjectModel('Nuggets') private readonly nuggetsModel: Model<NuggetsModel>
  ){}

  async getAllNuggets(): Promise<NuggetsModel[]> {
    const nuggets = await this.nuggetsModel.find().exec();
    return nuggets;
  }

  async getNuggets(id: string): Promise<any>{
    try {
      const nuggets = await this.nuggetsModel.findById(id);
      return nuggets;
    } catch (err) {
      throw new NotFoundException('Could not find the nuggets');
    }
  }
}
