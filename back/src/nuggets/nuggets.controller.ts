import { Controller, Get, Param } from '@nestjs/common';
import { NuggetsService } from './nuggets.service';

@Controller('nuggets')
export class NuggetsController {

  constructor(private readonly nuggetsService: NuggetsService){}

  @Get()
  getAllNuggets(): any{
    return this.nuggetsService.getAllNuggets();
  }

  @Get(':id')
  getNuggets(@Param('id') id: string) {
    return this.nuggetsService.getNuggets(id);
  }
}
